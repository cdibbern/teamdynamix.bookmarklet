(function() {
if (window.uiowaITSTDBookmarklet)
	return;
window.uiowaITSTDBookmarklet = true;
var $base = $("iframe").contents();
$base.find("body tr .TDGroupingRow:first-child")
	.click(function() { var row = $(this).parent().next(); while (row.length > 0 && row.find("td.TDGroupingRow").length == 0) { row.toggle(); row = row.next(); } })
	.attr('title', 'Click to expand/contract.')
	.css('cursor', 'pointer');
var btnShowAll = $('<button type="button" class="btn btn-primary" title="Show All"><i class="fa fa-plus"></i><span class="hidden-xs padding-left-xs">Show All</span></button>');
var btnHideAll = $('<button type="button" class="btn btn-primary" title="Hide All"><i class="fa fa-minus"></i><span class="hidden-xs padding-left-xs">Hide All</span></button>');
var filter = $('<input type="text" placeholder="Project name filter" />');
var filterResults = $('<span style="margin-left: 1em;"></span>');
btnShowAll.click(function() { filter.val(""); $base.find("tr").filter(function(i, tr) { return $base.find(tr).find(".TRItemTitleCell").length > 0 } ).show(); });
btnHideAll.click(function() { $base.find("tr").filter(function(i, tr) { return $base.find(tr).find(".TRItemTitleCell").length > 0 } ).hide(); });
var last = "";
filter.on("keyup change", function() {
	var s = filter.val();
	$base.find("tr").filter(function(i, tr) { return $base.find(tr).find(".TRItemTitleCell").length > 0 } ).hide();
	var num = $base.find("tr").filter(function(i, tr) { return $base.find(tr).find(".TRItemTitleCell").text().toUpperCase().indexOf(s.toUpperCase()) >= 0 } ).show().length;
	filterResults.text("Found: " + num);
	last = s;
});
$base.find("#modalBody").load(function() { filter.val(last); console.log("modal"); } );
$base.find("#divButtons").append(btnShowAll, btnHideAll, filter, filterResults);
})();