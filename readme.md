# TeamDynamix Bookmarklet
This bookmarklet adds some UI helpers to TeamDynamix.

- Hide All/Show All - show or hide all project subtasks.
- Project row expand/collapse.
- Filter by project name.

![Bookmarklet preview](https://git.uiowa.edu/cdibbern/teamdynamix.bookmarklet/raw/master/bookmarklet.png)

## Instructions

To add this bookmarklet in Chrome:

1. Right-click the bookmark bar.
2. Choose "Add page..."
3. Give it a name, something like "TeamDynamix Helpers"
4. Manually type (for security reasons) `javascript:` into the page URL box.
5. Paste the contents of [td-bookmarklet.js](https://git.its.uiowa.edu/users/cdibbern/repos/teamdynamix.bookmarklet/browse/td-bookmarklet.js) following the `javascript:`
6. Save the bookmarklet.
7. Open TeamDynamix
8. While on the TeamDynamix tab, click your bookmarklet to run it.

Just repeat 7 and 8 to run the bookmarklet whenever needed.

## Bugs/help wanted

This was written over a lunch hour. I happen to know all ITS employees have lunches (except Steve Bowers, on occasion).
If you feel like polishing this up a bit, here are some help wanted items:

1. Refresh the filter after saving hours (iframes are weird; no one likes them).
2. Re-add the clickable-rows after saving hours (iframes are terrible. Thanks a lot, [Scott Isaacs](https://en.wikipedia.org/wiki/Scott_Isaacs)).

Only kidding. Mr. Isaacs started Ajax/Web 2.0. We couldn't do much of what we do without him.
